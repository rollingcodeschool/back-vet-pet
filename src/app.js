import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";

import database from "./config/database";
import indexRouter from "./routes/index";
import registerRouter from "./routes/register";
import loginRouter from "./routes/login";
import appointmentRouter from "./routes/appointments";
import productsRouter from "./routes/products";
import salesRouter from "./routes/sales";
import faqRouter from "./routes/faq";
import AuthToken from "./middelwares/AuthToken";
import mercadopago from "mercadopago"


const app = express();
database.connect();


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

mercadopago.configure({
  access_token: 'TEST-261795224719371-021500-10b9e0d083de9becee9eca3931c2a065-227605443'
})

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "../public")));


app.use("/", indexRouter);
app.use("/register", registerRouter);
app.use("/login", loginRouter);
app.use("/appointments", appointmentRouter);
app.use("/products", productsRouter);
app.use("/sales", salesRouter);
app.use("/faq", faqRouter);


export default app;
