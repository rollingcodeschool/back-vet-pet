import mongoose from 'mongoose';

const faqSchema = new mongoose.Schema({
  name:{
    type: String,
    required: false
  },
  email: {
    type: String,
    required: true
  },
  inquire: {
    type: String,
    required: true
  },
  reply: {
    type: String,
    required: false
  },
  answeredState:{
    type:Boolean,
    default: false,
    required: true
  }
});

const Faq = mongoose.model('Faq', faqSchema);

module.exports = Faq;