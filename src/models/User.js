import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  lastName: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  dni: {
    type: Number,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true,
    default: "user"
  }
});

const User = mongoose.model('User', userSchema);

module.exports = User;