import jwt from "jsonwebtoken";
import User from '../models/User';
import CONFIG from "../config/config";

module.exports = function(req, res, next) {
  if (checkPath(req.path)) {
    if (req.headers.authorization) {
      let token = req.headers.authorization.split(" ")[1];
      jwt.verify(token, CONFIG.SECRET_TOKEN, function(error, decoded) {
        if (error) {
          return res.status(403).send({
            msg: "Heyy no tienes los permisos suficientes para estar aquí... Token inválido",
            error
          });
        }
        if (!decoded.user) {
          return res.status(403).send({
            msg: "Heyy no tienes los permisos suficientes para estar aquí... Token inválido, user not found"
          });                    
        }
        User.findOne({ _id: decoded.user._id }, function (err, user) {
          if (err) {   
            return res.status(403).send(err);
          }
          res.locals = {
            _id: decoded.user._id,
            role: decoded.user.role
          };
          next();
        });
      });
    } else {
      res.status(403).send({
        msg: "No tienes los permisos suficientes para estar aquí... hoalalal 🤢🤢🤢"
      });
    }
  } else next();
};

function checkPath(path) {
    return !(path == "/login" || path == "/register" || (path.indexOf("/products/search") >= 0))
}
