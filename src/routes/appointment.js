import express from 'express';
var router = express.Router();
import Appoint from '../models/Appointment';
import database from '../config/database';
import moment from 'moment';


database.connect();

router.get('/:date', (req, res) =>{
  const date = moment(req.params.date).toDate();
  console.log(date);

  Appoint.find({
    startTime: {
      $gte: date,
      $lte: moment(date).endOf('day')
    }
  }, null, (err, appoints) => {
    if(err) {
      res.status(500).send(err)
      return;
    }
    res.send({appointments: appoints})
  })
})

router.post('/', (req, res) => {
  Appoint.findOne({ startTime: req.body.startTime }, function (err, resp) {
    if (err) {
      res.send(err);
      return;
    }
    if (resp !== null) {
      res.status(502).json({ msg: 'Turno ya reservado, elija otro módulo disponible' });
      return;
    }

    console.log('%c⧭', 'color: #00e600', req.body);
    const newAppoint = new Appoint({
      startTime: req.body.startTime
    })
    console.log('%c⧭', 'color: #f2ceb6', newAppoint);

    newAppoint.save((err, appoint) => {
      if (err) {
        res.send(err);
      } else {
        res.send(appoint);
        console.log('Turno reservado a las: ', appoint);
      }
    })
  });
})  
  
  export default router;