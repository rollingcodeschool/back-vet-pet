import express from 'express';
var router = express.Router();
import bcrypt from 'bcryptjs';
import User from '../models/User';
import database from '../config/database';


// Ruta para el registro de un usuario
router.post('', async (req, res, next) => {
  User.findOne({ email: req.body.email }, function(err, resp){    
    if(err){
      res.send(err);
      return;
    }
    if (resp ) {
      res.status(503).json({msg:"El mail ya existe"});
      return;
    }
    const newUser = new User({
      lastName: req.body.lastName,
      name: req.body.name,
      dni: req.body.dni,
      email: req.body.email,
      password: req.body.password,
    });
    bcrypt.hash(newUser.password, 10, function(error, hash) {
      if(error){
        res.send(error);
      }
      if(hash) {
        newUser.password = hash;
        newUser.save((err,data)=>{
          if(err){
            res.send(err);
          }else{
            res.send(data);
            console.log('el nuevo usuario es:', data);
          }
        })
      }
    });
  });
})

export default router;
