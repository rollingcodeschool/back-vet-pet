# Vet Pet Back

Backend TFI Rolling Code School
Express + MongoDB

# *Lineamientos y procedimientos del PFI FullStack - Veterinaria*


# GIT

En el repositorio se trabajará de la siguiente forma:

1. - **Al comenzar una tarea se debe crear una branch de master.**
2. - **Terminada la tarea, mergear con la rama dev para probar que no haya errores.**
3. - **Si no hay errores, generar un Pull Request para el mergeo con la branch del sprint en curso, para que sea revisada y aprobada.**



# CODIFICACION

Para que cada respectivo Pull Request sea aceptado, deberá cumplir con las siguientes condiciones:

- **Identar con dos espacios todo el código.**
- **Nombrar variables y funciones de forma coherente.**
- **Evitar el uso de var para declarar variables, utilizar const(para constantes, arrays y objetos) y let según corresponda.**
- **Evitar comentarios innecesarios.**
- **NO debe haber console.logs en el código.**

# RUTAS A CONSULTAR

1. - **REGISTRO**
    
Ruta: http://localhost:4000/register 

 La consulta debe ir en el body de tomando los siguientes campos, los valores asignados son a modo de ejemplo.
 
    {
    "lastName":"Ibarra",
    "name":"Federico",
    "dni":"4709845",
    "email":"fede@hotmail.com", 
    "password":"123456"
    }

2. - **LOGIN**
    
Ruta: http://localhost:4000/login 

 La consulta debe ir en el body de tomando los siguientes campos, los valores asignados son a modo de ejemplo.
 
    {
    "email":"fede@hotmail.com", 
    "password":"123456"
    }

2. - **PRODUCTS**
    
Ruta: http://localhost:4000/products/search/all

 La consulta debe ir en el body de tomando los siguientes campos, los valores asignados son a modo de ejemplo.
 
    

    
